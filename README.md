# Buscador de recetas. ANGULAR

## Pasos realizados

Comencé por escribir la estructura que seguiría la página y a continuación dibuje los bocetos tanto de la estructura como del logotipo que usaría. Después de hacer esto, cree la guía de estilos a seguir y el diseño prototipo utilizando Adobe Xd. Paralelamente inicié el repositorio en GitLab e instalé Angular utilizando Node.js. Cuando terminé la fase de diseño, comencé a estructurar la carpeta del proyecto y creé los diferentes componentes que usaría, además de las carpetas para modelos y servicios. Conseguí inyectar el contenido de la API parseando la información, pero no fuí capaz de realizar la funcionalidad de busqueda. Después de esto, trasladé el diseño del prototipo a el proyecto.