export class Recetas {
    title: string;
    href?: string;
    ingredients: string;
    thumbnail: string;
}