import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { RecetasComponent } from './components/recetas/recetas.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { RecetaComponent } from './components/receta/receta.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    RecetasComponent,
    SearchBarComponent,
    RecetaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

  constructor () {
    
  }
}

