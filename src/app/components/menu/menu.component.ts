import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  toggleMenu() {

    let menu = document.getElementById("menu");
    let menuBtn = document.getElementById("menuBtn");

    menuBtn.classList.toggle("minus");
    menu.classList.toggle("show");

  }

  constructor() { }

  ngOnInit() {

  }

}
