import { Component, OnInit, Input } from '@angular/core';
import { Recetas } from "src/app/models/Recetas";

@Component({
  selector: 'app-receta',
  templateUrl: './receta.component.html',
  styleUrls: ['./receta.component.scss']
})
export class RecetaComponent implements OnInit {

  @Input() recetas: Recetas;

  constructor() { }

  ngOnInit() {
  }

}
