import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  searchDisplay() {
    let searchBar = document.getElementById("searchBar");
    searchBar.classList.add("change-display");

  }

  searchOut() {
    let searchBar = document.getElementById("searchBar");
    searchBar.classList.remove("change-display");
  }

  constructor() { }

  ngOnInit() {

    window.onscroll = function () { beSticky() };

    var searchBar = document.getElementById("searchBar");
    var menu = document.getElementById("menu");
    var sticky = searchBar.offsetTop;

    function beSticky() {
      if (window.pageYOffset > sticky) {
        searchBar.classList.add("sticky")
        menu.classList.add("sticky")
        
      } else {
        searchBar.classList.remove("sticky");
        menu.classList.remove("sticky");
      }
    }
  }

}
