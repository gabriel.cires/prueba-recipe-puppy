import { Component, OnInit } from '@angular/core';
import { Recetas } from "../../models/Recetas";
import { RecipesService } from "../../services/recipes.service";

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.component.html',
  styleUrls: ['./recetas.component.scss']
})
export class RecetasComponent implements OnInit {
  recetas:Recetas[];

  constructor(private recipeService:RecipesService) { }

  ngOnInit() {
    this.recetas = this.recipeService.getRecipes()
  }

}
